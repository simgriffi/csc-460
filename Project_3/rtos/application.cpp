/*#include "application.h"
#include "os.h"*/

#include <avr/io.h>
#include <util/delay.h>
#include "os.h"
#include "kernel.h"
//#include "usart.h"

#include "profiler.h"

// The test we want to run
#define USE_MAIN

#include "tests/test01.cpp"
#include "tests/test02.cpp"
#include "tests/test03.cpp"
#include "tests/test04.cpp"
#include "tests/test05.cpp"
#include "tests/test06.cpp"
#include "tests/test07.cpp"
#include "tests/test08.cpp"
#include "tests/test09.cpp"
#include "tests/test10.cpp"
#include "tests/test11.cpp"
#include "tests/test12.cpp"
#include "tests/test13.cpp"


#ifdef USE_MAIN
	/*DDRC = 1 << 7;
    PORTC = 0;
    _delay_ms(50);*/


    //PORTC = 0;

SERVICE *service;

void system(){
	int16_t val;
	Service_Subscribe(service, &val);
	DDRC = _BV(PC3);
	_delay_ms(3);
	DDRC ^= _BV(PC3);
	Task_Next();
}

void periodic1(){
	int i = 0;
	for(;;){
		PORTC = _BV(PC0);
		_delay_ms(3);	
		if(i == 10){
			Service_Publish(service, 0);
	    }
	    i++;
	    PORTC ^= _BV(PC0);
	    Task_Next();
	}
}

void rr(){
	for(;;)
	{
		PORTC = _BV(PC7);
		_delay_ms(5);
		PORTC ^= _BV(PC7);
	}   
}

extern int r_main(){    
    //usart_init();
    service = Service_Init();
    DDRC = _BV(PC3) | _BV(PC0) | _BV(PC7);
    Task_Create_System(system, 1);
    Task_Create_Periodic(periodic1, 0, 20, 2, 3);
    //Task_Create_RR(rr,0);

    return 0;
}

#endif
