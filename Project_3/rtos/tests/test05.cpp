#ifdef TEST_5

/*
 * Creates a periodic task that exceeds its wcet.
 * Should end in Error state
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

void periodic1(){
	int i;
	for(i=0;i<10;i++){
		_delay_ms(500);
		Task_Next();
	}
}


extern int r_main(){    
    usart_init();

    Task_Create_Periodic(periodic1,1, 5, 1, 2);
	
    return 0;
}

#endif