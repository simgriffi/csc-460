#ifdef TEST_3

/*
 * Checks to see if RR tasks can be created, and if they run in the expected order.
 * Prints 'F' if tasks execute in the wrong order. Prints 'C' when complete.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

int trace[6];
int trace_index = 0;

void check_trace(){
	int key[6] = {1,2,1,2,1,2};
	int i;
	for(i=0;i<6;i++){
		if(trace[i] != key[i]){
			usart_send('F');
		}
	}
	usart_send('C');
}

void rr(){
	int i;
	int arg = Task_GetArg();
	for(i=0; i<3; i++){
		trace[trace_index++] = arg;
		//usart_send((char)((int)'0' + arg));
		Task_Next();
	}
	if(arg == 1){
		Task_Create_RR(check_trace,0);
	}
}

extern int r_main(){    
    usart_init();

    Task_Create_RR(rr,1);
    Task_Create_RR(rr,2);

    return 0;
}

#endif