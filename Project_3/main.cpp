/*
 * RoombaVer3.c
 *
 * Created: 2015-03-05 13:32:56
 *  Author: Daniel
 */ 

//#define F_CPU 16000000UL

#include <avr/io.h>
#include "rtos/os.h"
#include "roomba/roomba.h"
#include "roomba/roomba_sci.h"
#include "radio/radio.h"
#include "ir/ir.h"
#include "cops_and_robbers.h"
#include "uart/uart.h"
#include "radio/sensor_struct.h"
#include "rtos/usart.h"

SERVICE* radio_receive_service;
SERVICE* ir_receive_service;
volatile uint8_t is_roomba_timedout = 0;

COPS_AND_ROBBERS roomba_num = ROOMBA1;
volatile ROOMBA_STATUSES current_roomba_status = ROOMBA_NOT_IT;
volatile uint8_t last_ir_value = 0;
volatile uint8_t state_changed = 0;

uint8_t station_addr[5] = {0xDB, 0xDB, 0xDB, 0xDB, 0xDB};

//Used to stop the Roomba when it gets shot.
void roomba_Drive( int16_t velocity, int16_t radius )
{
	Roomba_Send_Byte(DRIVE);
	Roomba_Send_Byte(HIGH_BYTE(velocity));
	Roomba_Send_Byte(LOW_BYTE(velocity));
	Roomba_Send_Byte(HIGH_BYTE(radius));
	Roomba_Send_Byte(LOW_BYTE(radius));
}

void radio_rxhandler(uint8_t pipenumber) {
	Service_Publish(radio_receive_service, 0);
}

//Handle expected IR values, change state if roomba is shot
void ir_rxhandler() {
	uint8_t ir_value = IR_getLast();
	if(ir_value == IR_SHOOT) {
		state_changed = 1;
		current_roomba_status = ROOMBA_JUST_TAGGED;
	} else {
		last_ir_value = ir_value;
	}
}

//This one can send info about who's it
void sendStatusPacket() {
	if(state_changed == 1){
		radiopacket_t packet;
		Radio_Set_Tx_Addr(station_addr);
		packet.type = ROOMBA_STATUS_UPDATE;
		packet.payload.status_info.roomba_number = roomba_num;
		Radio_Transmit(&packet, RADIO_RETURN_ON_TX);
		state_changed = 0;
	}	
}

void handleRoombaPacket(radiopacket_t *packet) {
	//Filter out unwanted commands.
	if (packet->payload.command.command == START ||
		packet->payload.command.command == BAUD ||
		packet->payload.command.command == SAFE ||
		packet->payload.command.command == FULL ||
		packet->payload.command.command == SENSORS)
	{
		return;
	}
	
	//Pass the command to the Roomba.
	PORTB |= (1 << PB7);
	Roomba_Send_Byte(packet->payload.command.command);
	for (int i = 0; i < packet->payload.command.num_arg_bytes; i++)
	{
		Roomba_Send_Byte(packet->payload.command.arguments[i]);
	}
		
	//Send status to base station is state has changed
	sendStatusPacket();
}

void it_blaster() {
	//Transmit IR data
	for(;;)
	{
		if(current_roomba_status == ROOMBA_IT)
		{
			cli();
			IR_transmit(IR_SHOOT);//packet->payload.ir_command.ir_data);
			sei();
		}
		Task_Next();
	}
}

//Spin the roomba in a circle
void super_spinny_move() {
	cli();
	roomba_Drive((uint16_t)500, (uint16_t) (-1));
	_delay_ms(2000);
	roomba_Drive(0, 500);
	roomba_Drive(0, 500);
	sei();
}

//Set status LED pins high/low depending on whether Roomba is 'it' or not
void set_status_led() 
{
	if(current_roomba_status == ROOMBA_IT)
		{
			PORTB |= (1 << PB6);
			PORTB &= ~(1 << PB7);
		} 
		else
		{
			PORTB &= ~(1 << PB6);
			PORTB |=  (1 << PB7);
		}
}

void rr_roomba_controller() 
{
	//Start the Roomba for the first time.
	int x;
	Roomba_Init();
	
	for(;;) {
		Service_Subscribe(radio_receive_service, &x);
		
		//Handle the packets
		RADIO_RX_STATUS result;
		radiopacket_t packet;
		do {
			result = Radio_Receive(&packet);
			if(result == RADIO_RX_SUCCESS || result == RADIO_RX_MORE_PACKETS) {	

				//Handle Roomba Commands
				if(packet.type == COMMAND) {
					//Check if we are changing to or from 'it' state
					if(packet.payload.command.it == roomba_num && current_roomba_status != ROOMBA_IT){
						current_roomba_status = ROOMBA_IT;
						super_spinny_move();
					}
					else if(current_roomba_status == ROOMBA_IT && packet.payload.command.it != roomba_num){
						current_roomba_status = ROOMBA_NOT_IT;
					}
					//Send command to roomba
					handleRoombaPacket(&packet);
				}	
			}
		} while (result == RADIO_RX_MORE_PACKETS);

		set_status_led();
	}
}

int r_main(void)
{
	//Turn off radio power.
	DDRL |= (1 << PL2);
	PORTL &= ~(1<<PL2);
	_delay_ms(500);
	PORTL |= (1<<PL2);
	_delay_ms(500);
	
	//Initialize output led pins
	DDRB = _BV(DDB7) | _BV(DDB6);
	set_status_led();

	//Initialize radio and infrared.
	cli();

	Radio_Init();
	IR_init();
	Radio_Configure_Rx(RADIO_PIPE_0, ROOMBA_ADDRESSES[roomba_num], ENABLE);
	Radio_Configure(RADIO_2MBPS, RADIO_HIGHEST_POWER);

	radio_receive_service = Service_Init();
	ir_receive_service = Service_Init(); 
	
	sei();
	
	Task_Create_RR(rr_roomba_controller,0);
	Task_Create_Periodic(it_blaster, 0, 100, 95, 0);

 	return 0;
}