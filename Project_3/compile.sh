#sudo avrdude -p m2560 -c stk500 -P /dev/tty.usbmodem621 -U flash:w:main.hex
##SEE SERIAL OUTPUT WITH: sudo cu -l /dev/tty.usbmodem1411 -s 9600 OR screen /dev/tty.usbmodem1411 9600

echo "Cleaning..."
rm -f *.o
rm -f *.elf
rm -f *.hex

echo "Compiling..."

avr-gcc -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o usart.o rtos/usart.cpp

avr-gcc -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o uart.o uart/uart.cpp

avr-g++ -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o main.o main.cpp

avr-g++ -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o os.o rtos/os.cpp

avr-g++ -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o radio.o radio/radio.cpp

avr-g++ -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o spi.o radio/spi.cpp

avr-g++ -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o ir.o ir/ir.cpp

avr-g++ -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o roomba.o roomba/roomba.cpp

avr-g++ -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -c -o cops_and_robbers.o cops_and_robbers.cpp


echo "Linking..."
#avr-g++ -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -g -o out.elf os.o uart.o radio.o spi.o ir.o Roomba.o
avr-gcc -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -o out.elf os.o cops_and_robbers.o spi.o radio.o usart.o main.o uart.o roomba.o ir.o
#avr-gcc -Wall -O2 -DF_CPU=16000000UL -mmcu=atmega2560 -g -o out.elf os.o 

echo "Making ELF..."
#avr-gcc -Wall -Os -mmcu=atmega2560 -g -o out.elf out.o


echo "Making HEX..."
avr-objcopy -j .text -j .data -O ihex out.elf main.hex
