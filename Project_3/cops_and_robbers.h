/*
 * cops_and_robbers.h
 *
 * Created: 2015-01-26 17:09:37
 *  Author: Daniel
 */ 


#ifndef COPS_AND_ROBBERS_H_
#define COPS_AND_ROBBERS_H_

#include "avr/io.h"

#define IR_SHOOT 65

typedef enum _roomba_nums {ROOMBA1 = 0, ROOMBA2, ROOMBA3, ROOMBA4} COPS_AND_ROBBERS;
extern uint8_t ROOMBA_ADDRESSES[][5];

extern uint8_t ROOMBA_FREQUENCIES[];

typedef enum _ir_commands{
	SEND_BYTE,
	REQUEST_DATA,
	AIM_SERVO
	} IR_COMMANDS;
	
typedef enum _roomba_statues{
	ROOMBA_IT,
	ROOMBA_JUST_TAGGED,
	ROOMBA_NOT_IT
	}ROOMBA_STATUSES;

#endif /* COPS_AND_ROBBERS_H_ */